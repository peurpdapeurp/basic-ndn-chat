
#include <ndn-cxx/face.hpp>

#include <iostream>
#include <string>

#include <sstream>

int counter = 0;
bool exiting = false;

namespace ndn {
  namespace examples {

    class Producer : noncopyable
    {
    public:
      void run() {
	m_face.setInterestFilter("/B",
				 std::bind(&Producer::onInterest, this, _1, _2),
				 RegisterPrefixSuccessCallback(),
				 std::bind(&Producer::onRegisterFailed, this, _1, _2));
	std::cout << "Type your messages and press enter" << std::endl
		  << "Type [Exit] to quit" << std::endl 
		  << "-----------------------------------" << std::endl;
	m_face.processEvents();
      }

    private:
      void onInterest(const InterestFilter& filter, const Interest& interest) {

	//std::cout << " >> I: " << interest << std::endl;

	Name dataName(interest.getName());
	dataName.append("testApp").appendVersion();

	char words[100];
	fgets(words, 100, stdin);
	std::string content(words);

	if (content == "[Exit]\n") {
	  std::cout << "Exiting..." << std::endl;
	  exiting = true;
	}

	shared_ptr<Data> data = make_shared<Data>();
	data->setName(dataName);
	data->setFreshnessPeriod(time::seconds(10));
	data->setContent(reinterpret_cast<const uint8_t*>(content.c_str()), content.size());

	m_keyChain.sign(*data);

	//std::cout << " >> Data: " << *data << std::endl;

	m_face.put(*data);
	
	if (exiting)
	  exit(0);
      }

      void onRegisterFailed(const Name& name, const std::string& reason) {
	std::cout << "Failed to register " << name << " on local daemon hub (" << 
	  reason << ")" << std::endl;
	m_face.shutdown();
      }

      Face m_face;
      KeyChain m_keyChain;
    };

    class Consumer : noncopyable
    {
    public:
      void run() {
	std::stringstream ss;
	ss << counter;
	std::string numberString = ss.str();
	Name name("/A/" + numberString);
	Interest interest(name);
	interest.setInterestLifetime(time::seconds(1000));
	interest.setMustBeFresh(true);

	m_face.expressInterest(interest,
			       std::bind(&Consumer::onData, this, _1, _2),
			       std::bind(&Consumer::onNack, this, _1, _2),
			       std::bind(&Consumer::onTimeout, this, _1));

	//std::cout << "Sending interest " << interest << std::endl;

	m_face.processEvents();
      }

    private:
      void onData (const Interest& interest, const Data& data) {

	std::string message(reinterpret_cast<const char*>(data.getContent().value()), data.getContent().value_size());

	if (message == "[Exit]\n") {
	  std::cout << "Other user exited, terminating chat..." << std::endl;
	  exit(0);
	}

	std::cout << "Incoming Message : " << message;

	counter++;
      }

      void onNack (const Interest& interest, const lp::Nack nack) {
	//std::cout << "Received Nack for interest " << interest << " for reason " << nack.getReason() << std::endl;
      }

      void onTimeout (const Interest& interest) {
	std::cout << "Timeout for " << interest << std::endl;
      }

      Face m_face;
    };
  }
}

void *consumerFunction(void *argvp) {

  ndn::examples::Consumer consumer;

  try {
    while (true) {
      //std::cout << "Attempting to get message... " << std::endl;

      consumer.run();

      //sleep(5);
    }
  }
  catch (const std::exception& e) {
    std::cout << "ERROR: " << e.what() << std::endl;
  }

}

void *producerFunction(void *argvp) {

  ndn::examples::Producer producer;

  try {
    std::cout << "Attempting to start server... " << std::endl;

    producer.run();
  }
  catch (const std::exception& e) {
    std::cout << "ERROR: " << e.what() << std::endl;
  }

}

int main (int argc, char **argv) {

  pthread_t tid1;
  pthread_t tid2;

  pthread_create(&tid1, NULL, consumerFunction, NULL);
  pthread_create(&tid2, NULL, producerFunction, NULL);

  pthread_join(tid1, NULL);
  pthread_join(tid2, NULL);

  return 0;

}

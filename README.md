Usage
=====

WAF build system uses two build stages:

1. Configuration

        ./waf configure [optional-flags]

2. Build

        ./waf build
        # or just ./waf

After successful build, the compiled binaries will be located in `./build` folder and can be installed to standard location using `./waf install` command.

Basic NDN Chat
=================

Very basic NDN chat between two hosts. The two hosts have to run different host programs (i.e. host1 on one computer and host2 on the other) because
the producer and consumer pairs between the hosts use different prefixes.

Using the Routing Script
============================

Use nfd-start and then run the route.sh script with the ip of the host to connect to. For example:

nfd-start
./route.sh udp4://192.168.56.121
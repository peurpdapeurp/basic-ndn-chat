#!/bin/bash

if [ "$#" -lt 1 ]
then
  echo "Usage: ./route.sh udp4://ip"
fi

ip=$1

nfd-start

nfdc face create $ip

nfdc route add /A $ip

nfdc route add /B $ip
